#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>


struct Timer{
    clock_t (*begin)(void);
    void (*print_diff)(clock_t start);
};

clock_t begin(void);
void print_diff(clock_t start);

int main(void){
    int i;
    clock_t start;
    struct Timer timer = {begin,print_diff};
    start = timer.begin();
    for(i=0;i<10000000;i++);
    timer.print_diff(start);	
    return 0;
}

clock_t begin(void){
	return clock();
}

void print_diff(clock_t start){
    double diff;
    diff=clock()-start;
    diff/=CLOCKS_PER_SEC;
    printf("duration: %f seconds\n", diff);
}