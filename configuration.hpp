// C++ standard library includes
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

// CAF includes
#include "caf/all.hpp"
#include "caf/io/all.hpp"

// Boost includes
CAF_PUSH_WARNINGS
#include <boost/multiprecision/cpp_int.hpp>
CAF_POP_WARNINGS

// Own includes
#include "is_probable_prime.hpp"
#include "int512_serialization.hpp"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;
using std::unordered_map;

using boost::multiprecision::int512_t;

struct config : actor_system_config {
    string host = "localhost";
    uint16_t port = 3030;
    size_t num_workers = 1;
    string mode = "manager";
    config() {
        add_message_type<int512_t>("int512_t");
        add_message_type<vector<int512_t>>("vector<int512_t>");
        opt_group{custom_options_, "global"}
        .add(host, "host,H", "server host (default: 'localhost')(ignored in coordinator mode)")
        .add(port, "port,p", "port (default: 3030)")
        .add(num_workers, "num-workers,w", "number of workers (default: 1)")
        .add(mode, "mode,m", "'coordinator' or 'manager' (default: 'manager')");
  }
};

using workerReady_atom  = atom_constant<atom("workerready")>;
using workerResult_atom = atom_constant<atom("workerresult")>;
