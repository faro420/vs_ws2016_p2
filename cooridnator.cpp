// C++ standard library includes
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <array>
#include <cassert>
#include <functional>
#include <mutex>

#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

#include <chrono>


// CAF includes
#include "caf/all.hpp"
#include "caf/io/all.hpp"

// Boost includes
CAF_PUSH_WARNINGS
#include <boost/multiprecision/cpp_int.hpp>
CAF_POP_WARNINGS

// Own includes
#include "is_probable_prime.hpp"
#include "int512_serialization.hpp"

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;

using boost::multiprecision::int512_t;

using namespace std;
using namespace caf;
using namespace std::chrono;



struct actorState {
    strong_actor_ptr atr;
    bool isIdle;
};

namespace coordinator {
    using workerReady_atom  = atom_constant<atom("workerrdy")>;
    using workerResult_atom = atom_constant<atom("workerres")>;
    using workerAmHere_atom = atom_constant<atom("workerher")>;
    using coordCalc_atom    = atom_constant<atom("coordcalc")>;
    using workerNoRes_atom  = atom_constant<atom("workernor")>;
    using coordNewNumber_atom = atom_constant<atom("coordnew")>;

    struct Timer{
        clock_t (*begin)(void);
        void (*print_diff)(clock_t start, double total, time_t starttime);
    };

    clock_t begin(void);
    void print_diff(clock_t start, double total, time_t starttime);

    struct state {
        int512_t numberToFactorize;
        int512_t partProduct = 1;
        int512_t currentHead;
		
        bool done = true;
		
        std::vector<int512_t> foundNumbers;				// List of found factors  (duplicates possible)
        std::vector<int512_t> foundProducts;			// List of found products (no duplicates)
        std::vector<struct actorState> connectedActors; // List of connected actors (workers)
        int512_t increment = 1;							// Current calculation increment
        struct Timer timer = { begin, print_diff };
        clock_t start;
	time_t starttime;
        double totalTimeUsedCPU = 0;					// Total time all workers used in sek
	high_resolution_clock::time_point startchrono;
        state() {
			// Init state
            std::istringstream iss{"1"};
            iss >> currentHead;
            iss >> numberToFactorize;
            iss >> partProduct;
            std::cout << "currentHead: " << currentHead << std::endl;
        }
    };

    // PROTOTYPES
    behavior init(stateful_actor<state>* self);
    behavior running(stateful_actor<state>* self);
    void addPart (stateful_actor<state>* self, int512_t part, bool fromProduct);
    void checkExistingProducts(stateful_actor<state>* self, int512_t part);
    int dispatchTask (stateful_actor<state>* self);
    bool checkIfDone(stateful_actor<state>* self);
    void dispatchSingle(stateful_actor<state>* self, actor target);
    void newNumber(stateful_actor<state>* self, int512_t newPrimToCalc);
    void displayElapsedTime( high_resolution_clock::time_point start );

    behavior init(stateful_actor<state>* self) {
        self->set_down_handler([=](const down_msg& msg) {
            auto end = self->state.connectedActors.end();
            int pos = 0;
            auto i = find_if(self->state.connectedActors.begin(), end,
                             [&](const struct actorState& atrState) {
                                 if(atrState.atr == msg.source) {
                                     return true;
                                 }
                                 else{
                                     pos++;
                                     return false;
                                 }
                             });
             if(i == end) {
                 aout(self) << "|=| WARNING: Lost connection to an unknown Worker?! ***" << endl;
             }
             else {
                 aout(self) << "|=| STATE: Lost connection to a worker! ***" << endl;
                 self->state.connectedActors.erase(self->state.connectedActors.begin() + pos);
             }
        });

        return running(self);
    }

    behavior running(stateful_actor<state>* self) {
        return {
            [=](coordNewNumber_atom, int512_t num){
                cout << "|=================================================" << endl;
                cout << "|###| Got new Number: " << num << endl;
                cout << "|=================================================" << endl;
                self->state.start = self->state.timer.begin();
		self->state.starttime = time(NULL);
		self->state.startchrono = high_resolution_clock::now();
                newNumber(self, num);
                dispatchTask(self);
            },
            [=](workerAmHere_atom, strong_actor_ptr worker) {
                if(!any_of(self->state.connectedActors.begin(),
                          self->state.connectedActors.end(),
                          [&](const struct actorState& atrState) {
                              return atrState.atr == worker;
                          })) {
                    aout(self) << "|=| STATE: New worker connected! ***" << endl;
                    struct actorState newWorker;
                    newWorker.isIdle = false;
                    newWorker.atr = worker;
                    self->state.connectedActors.emplace_back(newWorker);
                    self->monitor(actor_cast<actor>(worker));
                    return true;
                }
                else {
                    return false;
                }
            },
            [=](workerReady_atom, strong_actor_ptr worker) {
                //mtx.lock();
                bool isFound = false;
                for(struct actorState &atr : self->state.connectedActors){
                    if(atr.atr == worker) {

                        //int count = dispatchTask(self);
                        //self->send(actor_cast<actor>(i.atr), coordCalc_atom::value, self->state.currentHead);

                        if(!self->state.done) {
                            atr.isIdle = false;
                            aout(self) << "|=| STATE: Worker is ready, DISPATCHING! ***" << endl;
                            dispatchSingle(self, actor_cast<actor>(atr.atr));
                        }
                        else {
                            atr.isIdle = true;
                            aout(self) << "|=| STATE: Worker is ready, WAITING! ***" << endl;
                        }

                        isFound = true;
                        break;
                    }
                }

                 if(!isFound) {
                     aout(self) << "|=| WARNING: Non registered worker is ready, not registering him ***" << endl;
                 }
                 //mtx.unlock();
            },
            [=](workerResult_atom, strong_actor_ptr worker, int512_t base, int512_t result, double timeUsed) {
                if(self->state.done) return true;

                //mtx.lock();
                if(base == self->state.currentHead) {
                    // Found Number to current HEAD
                    cout << "| +Found[wkCH]: " << result << " [" << timeUsed << "s]" << endl;
                    self->state.totalTimeUsedCPU += timeUsed;
                    addPart(self, result, false);
                    checkExistingProducts(self, result);
                }
                else if(!any_of(self->state.foundNumbers.begin(), self->state.foundNumbers.end(),
                               [&](const int512_t& curNumber) {
                                   return curNumber == result;
                               })){
                   // Found Number from other HEAD is not in result list
                   cout << "| +Found[wkOH]: " << result << " [" << timeUsed << "s]" << endl;
                   self->state.totalTimeUsedCPU += timeUsed;
                   addPart(self, result, false);
                   checkExistingProducts(self, result);
                }
                else {
		    self->state.totalTimeUsedCPU += timeUsed;
                    aout(self) << "| -Invalid Result" << endl;
                }

                if(checkIfDone(self) && !self->state.done) {
                    cout << "|=================================================" << endl;
                    cout << "|===================FACTORS=======================" << endl;
                    cout << "| ";
                    for(int512_t num : self->state.foundNumbers) {
                        cout << num << "  ";
                    }
                    cout << endl;
                    cout << "|=================================================" << endl;

                    self->state.done = 1;

                    self->state.timer.print_diff(self->state.start, self->state.totalTimeUsedCPU, self->state.starttime);
		    displayElapsedTime( self->state.startchrono );
                }

                //mtx.unlock();
                return true;
            },
            [=](workerNoRes_atom, int512_t base, int512_t increment, double timeUsed) {
                if(self->state.currentHead == base) {
                    // Problem is current
                    self->state.increment = increment++;
                }
            }
        };
    }

    bool checkIfDone(stateful_actor<state>* self) {
        return !(self->state.partProduct < self->state.numberToFactorize);
    }

    void addPart (stateful_actor<state>* self, int512_t part, bool fromProduct) {
        if(part != 2 && !is_probable_prime(part)) {
            // Found part is no prime and may be added to product list
            if(!any_of(self->state.foundProducts.begin(), self->state.foundProducts.end(),
                           [&](const int512_t& curNumber) {
                               return curNumber == part;
                           })) {
                // Add new Product to list
                cout << "| $List!: " << part << endl;
                self->state.foundProducts.emplace_back(part);
            }
            else if(part % 2 == 0 && self->state.currentHead % 2 == 0) {
                addPart(self, 2, true);
            }

            self->state.increment++;
            return;
        }

        if(fromProduct) {
            cout << "| +Found[prod]: " << part << endl;
        }

        self->state.foundNumbers.emplace_back(part);
        self->state.partProduct *= part;
        self->state.currentHead = self->state.currentHead / part;
        self->state.increment = 1;

        // cout << "1## New last Part     : " << self->state.currentHead << " ##" << endl;
        // cout << "2## New partProduct: " << self->state.partProduct << " ##" << endl;

        if((is_probable_prime(self->state.currentHead) || self->state.currentHead == 2) && self->state.numberToFactorize != self->state.partProduct) {
            cout << "| +Found[last]: " << self->state.currentHead << endl;
            addPart(self, self->state.currentHead, false);
        }

        if(self->state.currentHead % part == 0) {
            cout << "| +Found[dupl]: " << part << endl;
            addPart(self, part, false);
        }
    }

    void checkExistingProducts(stateful_actor<state>* self, int512_t part){
        std::vector<int512_t> toCheck;

        for(int512_t &num : self->state.foundProducts) {
            if(num % part != 0 || num == 1 || num == part) continue;

            int512_t newPart = num;
            while(newPart % part == 0) {
                // Break down until it is no longe dividable through part, duplicates already gone
                newPart /= part;
            }

            num = newPart;
            if(newPart != 1 && !any_of(self->state.foundNumbers.begin(), self->state.foundNumbers.end(),
                           [&](const int512_t& curNumber) {
                               return curNumber == part;
                           })) {
                toCheck.emplace_back(newPart);
            }
        }

        for(int512_t num : toCheck) {
            addPart(self, num, true);
        }
    }

    int dispatchTask (stateful_actor<state>* self) {
        if(self->state.partProduct == self->state.numberToFactorize) return 0;

        int dispatchedCount = 0;
        for(struct actorState &atrSt : self->state.connectedActors) {
            if(atrSt.isIdle) {
                self->send(actor_cast<actor>(atrSt.atr), coordCalc_atom::value, self->state.currentHead, self->state.increment);
                dispatchedCount++;
                atrSt.isIdle = false;
            }
            else {
                //cout << "WORKER NOT IDLE" << endl;
            }
        }

        aout(self) << "|=| Dispatched task to " << dispatchedCount << " workers" << endl;
        return dispatchedCount;
    }

    void dispatchSingle(stateful_actor<state>* self, actor target) {
        if(self->state.partProduct != self->state.numberToFactorize) {
            self->send(target, coordCalc_atom::value, self->state.currentHead, self->state.increment);
            self->state.increment++;
        }
    }

    void newNumber(stateful_actor<state>* self, int512_t newPrimToCalc) {
        self->state.numberToFactorize = newPrimToCalc;
        self->state.partProduct = 1;
        self->state.currentHead = newPrimToCalc;
        self->state.foundNumbers.clear();
        self->state.foundProducts.clear();
        self->state.increment = 1;
        self->state.done = 0;
        self->state.totalTimeUsedCPU = 0;
    }

    clock_t begin(void){
    	return clock();
    }

    void print_diff(clock_t start, double total, time_t starttime){
        printf("| STATISTICS:\n");
	printf("| CPU-time: %f seconds\n", total);
    }

    void displayElapsedTime( high_resolution_clock::time_point start ){
	high_resolution_clock::time_point end = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( end - start ).count();
	
	cout << "| Realtime: " << ((double)duration)/(1000000) << " seconds" << endl;
    }
}
