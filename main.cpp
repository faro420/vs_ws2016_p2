
// C++ standard library includes
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

// CAF includes
#include "caf/all.hpp"
#include "caf/io/all.hpp"

// Boost includes
CAF_PUSH_WARNINGS
#include <boost/multiprecision/cpp_int.hpp>
CAF_POP_WARNINGS

// Own includes
#include "is_probable_prime.hpp"
#include "int512_serialization.hpp"

#include "manager.cpp"
#include "cooridnator.cpp"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;
using std::unordered_map;

using boost::multiprecision::int512_t;

using namespace caf;





namespace {


    using coordNewNumber_atom = atom_constant<atom("coordnew")>;


    struct config : actor_system_config {
        string host = "localhost";
        uint16_t port = 3033;
        size_t num_workers = 1;
        string mode = "manager";
        config() {
            add_message_type<int512_t>("int512_t");
            add_message_type<vector<int512_t>>("vector<int512_t>");
            opt_group{custom_options_, "global"}
            .add(host, "host,H", "server host (default: 'localhost')(ignored in coordinator mode)")
            .add(port, "port,p", "port (default: 3030)")
            .add(num_workers, "num-workers,w", "number of workers (default: 1)")
            .add(mode, "mode,m", "'coordinator' or 'manager' (default: 'manager')");
      }
    };

    void run_manager(actor_system& sys, const config& cfg) {
        auto usage = [] {
            cout << "Usage:" << endl
                 << "  quit                  : terminates the program" << endl
                 << "  exit                  : terminates the program" << endl
                 << "  connect <host> <port> : connects all to a new cooridinator" << endl
                 << endl;
        };
        usage();

        bool done = false;
        std::vector<actor> current_workers;

        if(!cfg.host.empty() && cfg.port > 0) {
            for(uint16_t i = 0; i < cfg.num_workers; i++) {
                auto worker = sys.spawn(worker::init);
                anon_send(worker, connect_atom::value, cfg.host, cfg.port);
                current_workers.emplace_back(worker);
            }
            cout << "*** Spawned " << cfg.num_workers << " workers ***" << endl;
        }
        else {
            cout << "*** No Host OR Port was defined => no workers have been started! ***" << endl;
        }

        message_handler eval {
            [&](string& cmd) {
                if(cmd != "quit" && cmd != "exit") {
                    return;
                }
                else {
                    for(uint16_t i = 0; i < current_workers.size(); i++) {
                        actor cur = current_workers.at(i);
                        //auto cur_act = actor_cast(cur);
                        anon_send(cur, exit_reason::user_shutdown);
                    }
                    current_workers.clear();
                }

                cout << "*** All workers shutdown! ***" << endl;
                done = true;
            },
            [&](string& arg0, string& arg1, string& arg2) {
                if(arg0 == "connect") {
                    char* end = nullptr;
                    auto lport = strtoul(arg2.c_str(), &end, 10);
                    if(end != arg2.c_str() + arg2.size()) {
                        cout << "\"" << arg2 << "\" is not an unsigned integer" << endl;
                    }
                    else if(lport > std::numeric_limits<uint16_t>::max()) {
                        cout << "\"" << arg2 << "\" > " << std::numeric_limits<uint16_t>::max() << endl;
                    }
                    else {
                        for(uint16_t i = 0; i < current_workers.size(); i++) {
                            actor cur = current_workers.at(i);
                            //auto cur_act = actor_cast(cur);
                            anon_send(cur, connect_atom::value, move(arg1), static_cast<uint16_t>(lport));
                        }
                    }
                }
            }
        };

        string line;
        while(!done && std::getline(std::cin, line)) {
            line = trim(std::move(line));
            std::vector<string> words;
            split(words, line, is_any_of(" "), token_compress_on);
            if(!message_builder(words.begin(), words.end()).apply(eval)) {
                usage();
            }
        }
    }

    void run_coordinator(actor_system& sys, const config& cfg){
        auto usage = [] {
            cout << "Usage:" << endl
                 << "  quit                  : terminates the program" << endl
                 << "  exit                  : terminates the program" << endl
                 << "  newnumber <number>    : run with new number" << endl
                 << "  example <index>       : run with number from example"
                 << endl;
        };
        usage();

        bool done = false;
        auto coord = sys.spawn(coordinator::init);

        cout << "*** Try publish at port " << cfg.port << " ***" << endl;
        auto expected_port = sys.middleman().publish(coord, cfg.port);
        if(!expected_port) {
            cerr << "!!!! publish failed: " << sys.render(expected_port.error()) << " !!!!" << endl;
            return;
        }

        cout << "*** Coordinator successfully publiched at port " << cfg.port << " ***" << endl;

        message_handler eval {
            [&](string& cmd) {
                if(cmd != "quit" && cmd != "exit") {
                    return;
                }
                else {
                    anon_send_exit(coord, exit_reason::user_shutdown);
                }

                cout << "*** Coordinator shutdown! ***" << endl;
                done = true;
            },
            [&](string& arg0, string& arg1) {
                if(arg0 == "newnumber") {
                    int512_t num;
                    std::istringstream iss{arg1};
                    iss >> num;
                    //cout << num << endl;
                    if(num > 0) {
                        anon_send(coord, coordNewNumber_atom::value, num);
                    }
                    else {
                        cout << "*** Number too small! ***" << endl;
                    }
                }
                else if(arg0 == "example") {
                    int512_t num;
                    if(arg1 == "1") {
                        std::istringstream iss{"8806715679"};
                        iss >> num;
                        anon_send(coord, coordNewNumber_atom::value, num);
                    }
                    else if(arg1 == "2") {
                        std::istringstream iss{"9398726230209357241"};
                        iss >> num;
                        anon_send(coord, coordNewNumber_atom::value, num);
                    }
                    else if(arg1 == "3") {
                        std::istringstream iss{"1137047281562824484226171575219374004320812483047"};
                        iss >> num;
                        anon_send(coord, coordNewNumber_atom::value, num);
                    }
                    else if(arg1 == "4") {
                        std::istringstream iss{"1000602106143806596478722974273666950903906112131794745457338659266842446985022076792112309173975243506969710503"};
                        iss >> num;
                        anon_send(coord, coordNewNumber_atom::value, num);
                    }
                    else {
                        cout << "Unknown example!" << endl;
                    }
                }
            }
        };

        string line;
        while(!done && std::getline(std::cin, line)) {
            line = trim(std::move(line));
            std::vector<string> words;
            split(words, line, is_any_of(" "), token_compress_on);
            if(!message_builder(words.begin(), words.end()).apply(eval)) {
                usage();
            }
        }
    }

// dispatches to run_* function depending on selected mode
void caf_main(actor_system &sys, const config &cfg) {
  if(cfg.mode == "manager") {
      run_manager(sys, cfg);
  }
  else if(cfg.mode == "coordinator") {
      run_coordinator(sys, cfg);
  }
  else
    cerr << "*** invalid mode specified" << endl;
}

} // namespace <anonymous>

CAF_MAIN(io::middleman)
