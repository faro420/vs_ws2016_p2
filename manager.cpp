
// C++ standard library includes
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

#include <chrono>

// CAF includes
#include "caf/all.hpp"
#include "caf/io/all.hpp"

// Boost includes
CAF_PUSH_WARNINGS
#include <boost/multiprecision/cpp_int.hpp>
//#include <boost/multiprecision/float128.hpp>
#include <boost/random.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
CAF_POP_WARNINGS

// Own includes
#include "is_probable_prime.hpp"
#include "int512_serialization.hpp"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;
using std::unordered_map;

using boost::multiprecision::int512_t;
//using boost::multiprecision::float128_type;




using namespace caf;
using namespace std::chrono;
using namespace boost::random;

namespace worker {
    struct Timer{
        clock_t (*begin)(void);
        double (*print_diff)(clock_t start);
    };

    clock_t begin(void);
    double print_diff(clock_t start);

    using workerReady_atom  = atom_constant<atom("workerrdy")>;
    using workerResult_atom = atom_constant<atom("workerres")>;
    using workerAmHere_atom = atom_constant<atom("workerher")>;
    using coordCalc_atom    = atom_constant<atom("coordcalc")>;
    using workerNoRes_atom  = atom_constant<atom("workernor")>;

    mt19937 mt;

    struct state {
        strong_actor_ptr current_coordinator;
        struct Timer timer = { begin, print_diff };
        clock_t start;
	high_resolution_clock::time_point startchrono;
    };

    // ========================
    // PROTOTYPES
    behavior unconnected(stateful_actor<state>* self);
    behavior running(stateful_actor<state>* self);
    void connecting(stateful_actor<state>* self, const std::string& host, uint16_t port);
    int512_t rho(stateful_actor<state>* self, int512_t N, int512_t a);
    double elapsedTime( high_resolution_clock::time_point start );

    behavior init(stateful_actor<state>* self) {
        self->set_down_handler([=](const down_msg& dm) {
            if(dm.source == self->state.current_coordinator) {
                aout(self) << "*** lost connection to coordinator! ***" << endl;
                self->state.current_coordinator = nullptr;
                self->become(unconnected(self));
            }
        });

        return unconnected(self);
    }

    behavior unconnected(stateful_actor<state>* self) {
        return {
            [=](connect_atom, const std::string& host, uint16_t port) {
                connecting(self, host, port);
            }
        };
    }

    behavior running(stateful_actor<state>* self, actor coordinator) {
        aout(self) << " ## Worker running! ##" << endl;
        self->request(coordinator, infinite, workerAmHere_atom::value, actor_cast<strong_actor_ptr>(self)).then(
            [=](bool isGood) {
                if(!isGood) {
                    aout(self) << "*** Coordinator seems to have problems... ***" << endl;
                }
                else {
                    self->send(coordinator, workerReady_atom::value, actor_cast<strong_actor_ptr>(self));
                }
            }
        );

        return {
            [=](coordCalc_atom, int512_t value, int512_t increment) {
                cout << "* Worker: got new value from coordinator: [" << value << "] *" << endl;
                //self->state.start = self->state.timer.begin();
                self->state.startchrono = high_resolution_clock::now();
		int512_t factor = rho(self, value, increment);

                //double timeUsed = self->state.timer.print_diff(self->state.start);
                double timeUsed = elapsedTime( self->state.startchrono );

                if(factor == 0) {
                    cout << "--- Found no factor ---" << endl;
                    self->request(coordinator, infinite, workerNoRes_atom::value, value, increment, timeUsed).then(
                        [=](bool isGood) {
                            ///
                        }
                    );
                }
                else {
                    self->request(coordinator, infinite, workerResult_atom::value, actor_cast<strong_actor_ptr>(self), value, factor, timeUsed).then(
                        [=](bool isGood) {
                            ///
                        }
                    );
                }

                self->send(coordinator, workerReady_atom::value, actor_cast<strong_actor_ptr>(self));
            }
        };
    }

    void connecting(stateful_actor<state>* self, const std::string& host, uint16_t port) {
        self->state.current_coordinator = nullptr;
        auto mm = self->system().middleman().actor_handle();
        self->request(mm, infinite, connect_atom::value, host, port).await(
            [=](const node_id&, strong_actor_ptr coord, const std::set<std::string>& ifs) {
                if(!coord) {
                    aout(self) << "*** No coordinator found at \"" << host << "\":" << port << endl;
                    return;
                }
                if(!ifs.empty()) {
                    aout(self) << "*** typed actor found at \"" << host << "\":" << port <<", but expected an untyped actor ***" << endl;
                    return;
                }
                aout(self) << "*** successfully connected to coordinator ***" << endl;
                self->state.current_coordinator = coord;
                auto hdl = actor_cast<actor>(coord);
                self->monitor(hdl);
                self->become(running(self, hdl));
            },
            [=](const error& err) {
                aout(self) << "*** cannot connect to \"" << host << "\":" << port << " => " << self->system().render(err) << endl;
                self->become(unconnected(self));
            }
        );
    }

    int512_t ggt(int512_t first, int512_t second) {
        int512_t a = first;
        int512_t b = second;
        int512_t h;
        while (b != 0) {
            h = a % b;
            a = b;
            b = h;
        }

        //cout << "ggt <" << first << "," << second << "> = " << a;

        return a;
    }

    int512_t rho(stateful_actor<state>* self, int512_t N, int512_t a) {
        uniform_int_distribution<int512_t>ui (0, N);
        //uniform_int_distribution<int512_t>ui2(1, )
        int512_t x = ui(mt);
        int512_t y = x;
        int512_t d;
        int512_t p = 1;

        int512_t cyclesForIncrement = sqrt(sqrt(N)); cyclesForIncrement += 10;
        int512_t cylceCount = 0;

        cout << "RANDOM: " << x << endl;

        do {
            cylceCount++;
            x = (x * x + a) % N;
            y = (y * y + a) % N;
            y = (y * y + a) % N;
            d = (y - x) % N;
            p = ggt(d, N);
            if(cylceCount > cyclesForIncrement) {
                return 0;
            }
        } while ( p == 1 || p == -1);

        if(p != N) {
            cout << "++ found factor: " << p << " after " << cylceCount << "[" << a << "]" <<endl;
            return p < 0 ? -p : p;
        }
        else {
            if(is_probable_prime(N)) {
                return N;
            }
            else {
                aout(self) << "-- need to work on... " << endl;
                return rho(self, N, a + 1);
            }
        }
    }

    clock_t begin(void){
    	return clock();
    }

    double print_diff(clock_t start){
        double diff;
        diff=clock()-start;
        diff/=CLOCKS_PER_SEC;
        //printf("duration: %f seconds\n", diff);
        return diff;
    }

    double elapsedTime( high_resolution_clock::time_point start ) {
	high_resolution_clock::time_point end = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( end - start ).count();

	return ((double)duration)/(1000000);
    }
}

string trim(std::string s) {
    auto not_space = [](char c) {
        return !isspace(c);
    };
    s.erase(s.begin(), find_if(s.begin(), s.end(), not_space));
    s.erase(find_if(s.rbegin(), s.rend(), not_space).base(), s.end());
    return s;
}
